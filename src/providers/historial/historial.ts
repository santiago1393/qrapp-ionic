
import { Injectable } from '@angular/core';

import { ScanData } from '../../models/scan-data.model';

import { InAppBrowser } from '@ionic-native/in-app-browser';

import { ModalController, Platform, ToastController } from "ionic-angular";

import { MapaPage } from "../../pages/mapa/mapa";

import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';

import { EmailComposer } from '@ionic-native/email-composer';





/*
  Generated class for the HistorialProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HistorialProvider {

  private _historial:ScanData[] = [];

  constructor(private toastctrl:ToastController,
              private platform:Platform, 
              private contacts:Contacts, 
              private iab:InAppBrowser,
              private modalCtrl:ModalController,
              private emailCtrl:EmailComposer) {
    console.log('Hello HistorialProvider Provider');
  }

  abrir_scan(index:number){

    let scanData = this._historial[index];

    switch (scanData.tipo) {
        case "http":
          this.iab.create(scanData.info, "_system");
        break;
        case "mapa":
          let modal = this.modalCtrl.create(MapaPage, {"coords": scanData.info});
          modal.present();
        break;
        case "vcard":
          this.crear_contacto(scanData.info);
        break;
        case "email":
          this.send_mail(scanData.info);
        break;
    
      default:
        console.error("Tipo no soportado");
        break;
    }

  }

  private send_mail(texto:string){
      // "MATMSG:TO:santi1393@hotmail.com;SUB:Prueba;BODY:Hola;;"
      let campos:string[] = texto.split(";");
      let mailto:string = campos[0].substring(10,campos[0].length);
      let subjectmail:string = campos[1].substring(4,campos[1].length);
      let bodymail:string = campos[2].substring(5,campos[2].length);

      console.log(mailto);
      console.log(subjectmail);
      console.log(bodymail);
   
      let email = {
        to: mailto,
        subject: subjectmail,
        body: bodymail,
        isHtml: false
      };  
      // Send a text message using default options

      this.emailCtrl.open(email);

     /* this.emailCtrl.isAvailable().then((available: boolean) =>{
 
    
        let email = {
          to: mailto,
          subject: subjectmail,
          body: bodymail,
          isHtml: true
        };    
        // Send a text message using default options
        this.emailCtrl.open(email);
       }).catch((error : any) =>
          {
             console.log('User does not appear to have device e-mail account');
             console.dir(error);
          });*/
     }

  private crear_contacto(texto:string){
    let campos : any = this.parse_vcard(texto);
    console.log(campos);  

    let nombre = campos.fn;
    let tel = (campos.tel[0].value[0]);
    
    if(!this.platform.is('cordova')){
      console.warn("Estoy en la PC, nno puedo crear contacto");
      return;
    }

    let contact: Contact = this.contacts.create();

    contact.name = new ContactName(null, nombre);
    contact.phoneNumbers = [new ContactField('mobile', tel)];
    contact.save().then( 
      ()=>this.crear_toast("Contacto: " + nombre + " creado"),
      (error)=>this.crear_toast("Error al crear contacto"));
  }

  private crear_toast(texto:string){
    this.toastctrl.create({message:texto, duration:1500}).present(); 
  }

  private parse_vcard( input:string ) {

    var Re1 = /^(version|fn|title|org):(.+)$/i;
    var Re2 = /^([^:;]+);([^:]+):(.+)$/;
    var ReKey = /item\d{1,2}\./;
    var fields = {};

    input.split(/\r\n|\r|\n/).forEach(function (line) {
        var results, key;

        if (Re1.test(line)) {
            results = line.match(Re1);
            key = results[1].toLowerCase();
            fields[key] = results[2];
        } else if (Re2.test(line)) {
            results = line.match(Re2);
            key = results[1].replace(ReKey, '').toLowerCase();

            var meta = {};
            results[2].split(';')
                .map(function (p, i) {
                var match = p.match(/([a-z]+)=(.*)/i);
                if (match) {
                    return [match[1], match[2]];
                } else {
                    return ["TYPE" + (i === 0 ? "" : i), p];
                }
            })
                .forEach(function (p) {
                meta[p[0]] = p[1];
            });

            if (!fields[key]) fields[key] = [];

            fields[key].push({
                meta: meta,
                value: results[3].split(';')
            })
        }
    });

    return fields;
}

  cargar_historial(){
    return this._historial;
  }

  insert_historial(texto:string){
    console.log(texto);
    let data = new ScanData(texto);
    console.log(data);
    this._historial.unshift(data);
    console.log(this._historial);

    // Envio el scan con index 0 porque al realizar un unshit al array el insert se agrega de primero, osea en la posicion 0
    this.abrir_scan(0);

  }

}
