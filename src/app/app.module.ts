import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage, TabsPage, GuardadosPage, MapaPage } from '../pages/index.paginas';

import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { HistorialProvider } from '../providers/historial/historial';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { EmailComposer } from '@ionic-native/email-composer';

import { AgmCoreModule } from '@agm/core';
import { Contacts, Contact } from '@ionic-native/contacts';

@NgModule({
  declarations: [
    MyApp,
    HomePage, TabsPage, GuardadosPage, MapaPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDsXQGaQLvoZR9ucbiXLZsQ8_SKhVRKp14'
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage, TabsPage, GuardadosPage, MapaPage
  ],
  providers: [
    StatusBar,
    BarcodeScanner,
    Contacts,
    Contact,
    InAppBrowser,
    SplashScreen,
    EmailComposer,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    HistorialProvider
  ]
})
export class AppModule {}
