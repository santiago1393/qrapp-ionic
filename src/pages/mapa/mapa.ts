import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { AgmCoreModule } from '@agm/core';

/**
 * Generated class for the MapaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mapa',
  templateUrl: 'mapa.html',
})
export class MapaPage {

  title: string = 'My first AGM project';
  lat: number;  
  lng: number;

  constructor(private viewCtrl:ViewController, public navCtrl: NavController, public navParams: NavParams) {
    this.lat= 51.678418;
    this.lng= 7.809007;

    let coords = this.navParams.get("coords").split(",");
    this.lat = Number( coords[0].replace("geo", ""));
    this.lng = Number(coords[1]);
    

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapaPage');
  }

  cerrar_modal(){
    this.viewCtrl.dismiss();
  }
}
