import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { HistorialProvider } from "../../providers/historial/historial";
import { ScanData } from "../../models/scan-data.model";

/**
 * Generated class for the GuardadosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-guardados',
  templateUrl: 'guardados.html',
})
export class GuardadosPage {

  _historial:ScanData[] = [];

  constructor(public historial: HistorialProvider, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this._historial = this.historial.cargar_historial();
    console.log('ionViewDidLoad GuardadosPage');
  }

  abrir_scan(index:number){

    this.historial.abrir_scan(index);
  }

}
