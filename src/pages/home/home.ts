import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { ToastController } from 'ionic-angular';

import {HistorialProvider} from "../../providers/historial/historial";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public historialSrv:HistorialProvider, public platform: Platform, public navCtrl: NavController, private barCode:BarcodeScanner, public toastCtrl:ToastController) {

  }

  escanear(){
    console.log("Start Scan");

    if(!this.platform.is('cordova')){
      
      // this.historialSrv.insert_historial("http://www.google.com");
      //this.historialSrv.insert_historial("geo:9.97606279727691,-84.0067932788208");
      //this.historialSrv.insert_historial( `BEGIN:VCARD
//VERSION:2.1
//N:Kent;Clark
//FN:Clark Kent
//ORG:
//TEL;HOME;VOICE:12345
//TEL;TYPE=cell:67890
//ADR;TYPE=work:;;;
//EMAIL:clark@superman.com
//END:VCARD` );
//      return;
//    }
    this.historialSrv.insert_historial( "MATMSG:TO:santi1393@hotmail.com;SUB:Prueba;BODY:Hola;;");
    return;
    }

    this.barCode.scan().then(barcodeData => {
      console.log('Barcode data: ', barcodeData.text);
      console.log('Barcode Format: ', barcodeData.format);
      console.log('Barcode Cancelled: ', barcodeData.cancelled);

      if(barcodeData.cancelled == false && barcodeData.text != null){
          this.historialSrv.insert_historial(barcodeData.text);
      }

     }).catch(err => {
         console.log('Error', err);
         this.present_error("Error: " + err)
     });

  }

  present_error(mensaje:string) {
    const toast = this.toastCtrl.create({
      message: mensaje,
      duration: 2500
    });
    toast.present();
  }


}
