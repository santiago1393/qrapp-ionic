export class ScanData{

    info:string;
    tipo:string;

    constructor(texto:string){
        this.info = texto;
        this.tipo = "no definido";
        if(texto.startsWith("http") || texto.startsWith("www")){
            this.tipo = "http";
        }else if(texto.startsWith("geo")){
            this.tipo = "mapa";
        }else if(texto.startsWith("BEGIN:VCARD")){
            this.tipo = "vcard";
        }else if(texto.startsWith("MATMSG")){
            this.tipo = "email";
            // "MATMSG:TO:santi1393@hotmail.com;SUB:Prueba;BODY:Hola;;"
        }
    }
}